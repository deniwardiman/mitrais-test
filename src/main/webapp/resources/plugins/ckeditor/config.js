/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.extraPlugins = 'button';
	config.extraPlugins = 'toolbar';
	config.extraPlugins = 'notification';
	config.extraPlugins = 'notificationaggregator';
	config.extraPlugins = 'filetools';
	config.extraPlugins = 'widgetselection';
	config.extraPlugins = 'clipboard';
	config.extraPlugins = 'lineutils';
	config.extraPlugins = 'widget';
	config.extraPlugins = 'uploadwidget';
	config.extraPlugins = 'uploadimage';
	config.imageUploadUrl = '/admin/ckeditor/upload';
	config.removePlugins = 'elementspath';

	config.toolbarGroups = [
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
		{ name: 'forms', groups: [ 'forms' ] },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
		{ name: 'links', groups: [ 'links' ] },
		{ name: 'insert', groups: [ 'insert' ] },
		'/',
		{ name: 'styles', groups: [ 'styles' ] },
		{ name: 'colors', groups: [ 'colors' ] },
		{ name: 'tools', groups: [ 'tools' ] },
		{ name: 'others', groups: [ 'others' ] },
		{ name: 'about', groups: [ 'about' ] }
	];

	config.removeButtons = 'Source,Save,NewPage,Preview,Print,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Undo,Redo,Replace,Find,SelectAll,Form,Scayt,Checkbox,Radio,TextField,Textarea,Button,Select,ImageButton,HiddenField,Superscript,Subscript,CopyFormatting,RemoveFormat,Outdent,Indent,Blockquote,CreateDiv,JustifyLeft,JustifyCenter,JustifyRight,JustifyBlock,BidiLtr,BidiRtl,Language,Anchor,Link,Unlink,Image,Table,Flash,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Styles,TextColor,Maximize,About,ShowBlocks,BGColor,Format,Font,FontSize';

	CKEDITOR.on('instanceReady', function (e) {
		var instance = e.editor;
		instance.on("change", function (evt) {
			onCKEditorChange(evt.editor);
		});
		//key event handler is a hack, cause change event doesn't handle interaction with these keys 
		instance.on('key', function (evt) {
			var backSpaceKeyCode = 8;
			var deleteKeyCode = 46;
			if (evt.data.keyCode == backSpaceKeyCode || evt.data.keyCode == deleteKeyCode) {
				//timeout needed cause editor data will update after this event was fired
				setTimeout(function() {
					onCKEditorChange(evt.editor);
				}, 100);
			}
		});
		instance.on('mode', function () {
			if (this.mode == 'source') {
				var editable = instance.editable();
				editable.attachListener(editable, 'input', function (evt) {
					onCKEditorChange(instance);
				});
			}
		});
	});
	
	function onCKEditorChange(intance) {
		intance.updateElement();
		triggerElementChangeAndJqueryValidation($(intance.element.$));
	}
	
	function triggerElementChangeAndJqueryValidation(element) {
		element.trigger('keyup');
	}

};
