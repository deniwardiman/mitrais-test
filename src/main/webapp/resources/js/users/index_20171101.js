var table;
$(document).ready(function(){
	$('#action').hide();

   table = $("#listTable").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "/admin/users/lists",
            "data": function(data) {
                data.filter = $("#searchFilter").val();
            }
        },
        language: {
            searchPlaceholder: "Search by title..."
        },
        columnDefs : [
            {
                'targets': 0,
                'searchable': false,
                'orderable': false,
                'sortable': false,
                'className': 'dt-body-center',
                'width': "20px",
                'render': function (data, type, full, meta){
                    return '<input type="checkbox" class="cb-action" name="id[]" value="' + $('<div/>').text(data).html() + '">';
                }
            },
            { targets: 1, sortable: false, width: "20px"},
            { targets: 4, sortable: true, width: "30px"},
            { targets: 5, sortable: false, width: "85px"},
        ],
        order: [[ 2, "asc" ]],
        "fnDrawCallback": function() {
            $('.dd-status').change(function() {
                var rowId = [parseInt($(this).prop('id'))];
                var val = [parseInt($(this).val())];
//                if (val == @Article.PUBLISH){
//                    window.location.replace("/admin/information/article/"+rowId+"/preview");
//                }else{
//                    updateStatus(rowId, val, false);
//                }
            })
        }
    });

    oTable = $('#listTable').DataTable();

    $('#listTable_length').hide();

    // Handle click on "Select all" control
   $('#select-all').on('click', function(){
      // Get all rows with search applied
      var rows = table.rows({ 'search': 'applied' }).nodes();
      // Check/uncheck checkboxes for all rows in the table
      //$('input[type="checkbox"]', rows).prop('checked', this.checked);
      $('.cb-action', rows).prop('checked', this.checked);
      if(this.checked){
        $('#action').show();
      }else{
        $('#action').hide();
      }
      //
   });

   // Handle click on checkbox to set state of "Select all" control

   $('#listTable tbody').on('change', '.cb-action', function(){
      // If checkbox is not checked
      if(!this.checked){
         var el = $('#example-select-all').get(0);
         // If "Select all" control is checked and has 'indeterminate' property
         if(el && el.checked && ('indeterminate' in el)){
            // Set visual state of "Select all" control
            // as 'indeterminate'
            el.indeterminate = true;
         }
      }

      if(getSelectedIds() != ''){
        $('#action').show();
      }else{
        $('#action').hide();
      }
   });

   // Handle action change
   $('#action').on('change', function(){
        if($('#action').val() == "delete"){
            $('#modal-delete-article').modal('show');
            $("#article_id").val(getSelectedIds());
            $('.modal-title').html('Delete Article');
            $('.modal-body').html('Are you sure you want to delete these article?');
        }else if($('#action').val() != ""){
            updateStatusAll($('#action').val());
        }
   });

   $('#searchFilter').on('change', function(){
        table.ajax.reload();
   });

});

function viewDetail(id){
    $('#detailModal-body-content').html("<div class=\"loader-panel panel\"><div class=\"loader\"></div></div>");
    $('#detailModal-body-content').load('/admin/users/detail-popup');
}