<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<!DOCTYPE html>
<html>
    <head>
            <c:url var="home" value="/" scope="request" />
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Mitrais - Registration</title>
        <%@ include file="/WEB-INF/views/include.jsp" %>
    </head>
    <body class="hold-transition skin-green sidebar-mini">
                <section class="content-header">
                    <h1>Registration Page</h1>
                    </ol>
                </section>
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-success">
                                <div class="box-body">
                                    <form id="formPromo" enctype="multipart/form-data" action="${pageContext.request.contextPath}/save-create" method="post" class="form-horizontal" role="form">
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-2">Email *</label>
                                                        <div class="col-sm-5">
                                                            <input type="number" class="form-control" id="mobileNumber" name="mobileNumber" placeholder="mobileNumber" value="" >
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2">First Name *</label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control" id="firstName" name="firstName" placeholder="First Name" value="" >
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2">Last Name *</label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Last Name" value="" >
                                                        </div>
                                                    </div>
                                                     <div class="form-group">
                                                        <label class="col-sm-2">Start Booking Date *</label>
                                                        <div class="col-sm-10">
                                                            <div class="form-group col-md-4">
                                                                <div class="input-group date">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </div>
                                                                    <input type="date" class="form-control pull-right date" id="birthDay" value="" name="birthDay">
                                                                </div>
                                                                    <!-- /.input group -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2">Gender</label>
                                                        <div class="col-sm-10">
                                                            <div class="radio">
                                                                <label>
                                                                    <input type="radio" name="gender" id="male" value="male">
                                                                        male
                                                                </label>
                                                                <label style="margin-left: 10px;">
                                                                    <input type="radio" name="gender" id="female" value="female" checked >
                                                                        female
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2">Email *</label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="" >
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2">Password *</label>
                                                        <div class="col-sm-5">
                                                            <input type="password" class="form-control" id="password" name="password" placeholder="Password" value="" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="modal-footer">
                                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                            <input type="hidden" id="id" name="id" value="${id}"/>
                                            <button type="submit" class="btn btn-success submit-save pull-left" name="submit1">Submit</button>
                                            <a href="/login" class="btn btn-danger pull-left">Back</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            <%@ include file="/WEB-INF/views/footer.jsp" %>
            <script type='text/javascript'>
                $(document).ready(function(){
                    $('#formPromo').validate({
                        ignore:[],
                        rules: {
                            mobileNumber: "required",
                            firstName: "required",
                            lastName: "required",
                            birthDay:"required",
                            email:"required",
                            password:"required"
                            }
                        });

                        });
                        </script>
    </body>
</html>
<style>
    .danger {
        color:#dd4b39;
        display: inline-block;
        max-width: 100%;
        margin-bottom: 5px;
        font-weight: 700;
    }
</style>



