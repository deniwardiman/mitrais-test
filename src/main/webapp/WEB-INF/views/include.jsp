<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel='stylesheet' href='<c:url value="/resources/css/bootstrap.min.css" />'>
<!-- Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jqueryui -->
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/ui-lightness/jquery-ui.css">

<link rel='stylesheet' href='<c:url value="/resources/plugins/datatables/dataTables.bootstrap.css" />'>
<link rel='stylesheet' href='<c:url value="/resources/css/bootstrap-toggle.css" />'>
<link rel='stylesheet' href='<c:url value="/resources/css/loader.css" />'>
<link rel='stylesheet' href='<c:url value="/resources/plugins/select2/select2.min.css" />'>
<!-- Theme style -->
<link rel='stylesheet' href='<c:url value="/resources/css/AdminLTE.min.css" />'>
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel='stylesheet' href='<c:url value="/resources/css/_all-skins.min.css" />'>
<!-- iCheck -->
<link rel='stylesheet' href='<c:url value="/resources/plugins/iCheck/flat/blue.css" />'>
<!-- Date Picker -->
<link rel='stylesheet' href='<c:url value="/resources/plugins/datepicker/datepicker3.css" />'>
<!-- Daterange picker -->
<link rel='stylesheet' href='<c:url value="/resources/plugins/daterangepicker/daterangepicker.css" />'>
<!-- Bootstrap time Picker -->
<link rel='stylesheet' href='<c:url value="/resources/plugins/timepicker/bootstrap-timepicker.min.css" />'>
<link rel='stylesheet' href='<c:url value="/resources/plugins/toastr/toastr.min.css" />'>
<!-- bootstrap wysihtml5 - text editor -->
<link rel='stylesheet' href='<c:url value="/resources/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" />'>
<link rel='stylesheet' href='<c:url value="/resources/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" />'>
<!-- additional css -->
<link rel='stylesheet' href='<c:url value="/resources/css/main.css" />'>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->



<!-- jQuery 2.2.3 -->
<script src='<c:url value="/resources/js/jquery-2.2.3.min.js" />' type='text/javascript'></script>
<!-- jQuery Validate -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script src='<c:url value="/resources/js/jquery.validate.min.js" />' type='text/javascript'></script>
<!-- jQuery UI 1.11.4 -->

<!-- Bootstrap 3.3.6 -->
<script src='<c:url value="/resources/js/bootstrap.min.js" />' type='text/javascript'></script>
<script src='<c:url value="/resources/js/bootstrap-toggle.js" />' type='text/javascript'></script>

<script src='<c:url value="/resources/plugins/datatables/jquery.dataTables.min.js" />' type='text/javascript'></script>
<script src='<c:url value="/resources/plugins/datatables/dataTables.bootstrap.min.js" />' type='text/javascript'></script>

<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src='<c:url value="/resources/plugins/daterangepicker/daterangepicker.js" />' type='text/javascript'></script>
<!-- datepicker -->
<script src='<c:url value="/resources/plugins/datepicker/bootstrap-datepicker.js" />' type='text/javascript'></script>
<!-- bootstrap time picker -->
<script src='<c:url value="/resources/plugins/timepicker/bootstrap-timepicker.min.js" />' type='text/javascript'></script>
<!-- Bootstrap WYSIHTML5 -->
<script src='<c:url value="/resources/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" />' type='text/javascript'></script>
<!-- Slimscroll -->
<script src='<c:url value="/resources/plugins/slimScroll/jquery.slimscroll.min.js" />' type='text/javascript'></script>

<!--<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>-->
<script type="text/javascript" src='<c:url value="/resources/plugins/ckeditor/ckeditor.js" />'></script>

<!-- bootbox -->
<script src='<c:url value="/resources/plugins/bootbox/bootbox.min.js" />' type='text/javascript'></script>

<script src='<c:url value="/resources/plugins/select2/select2.full.min.js" />' type='text/javascript'></script>
<!-- FastClick -->
<script src='<c:url value="/resources/plugins/chartjs/Chart.min.js" />' type='text/javascript'></script>
<script src='<c:url value="/resources/plugins/fastclick/fastclick.js" />' type='text/javascript'></script>
<script src='<c:url value="/resources/plugins/toastr/toastr.min.js" />' type='text/javascript'></script>
<!-- AdminLTE App -->
<script src='<c:url value="/resources/js/app.min.js" />' type='text/javascript'></script>
<script src='<c:url value="/resources/js/jquery.form.js" />' type='text/javascript'></script>
<script src='<c:url value="/resources/js/myapp.js" />' type='text/javascript'></script>
<!-- AdminLTE for demo purposes -->
<script src='<c:url value="/resources/js/demo.js" />' type='text/javascript'></script>
<script src='<c:url value="/resources/js/autonumeric.js" />' type='text/javascript'></script>