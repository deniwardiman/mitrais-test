<!-- /.content-wrapper -->
<style>
footer {
  position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;
    background-color: #00a65a;
    color: white;
    text-align: center;
    font-size:20px;
    a{
        color:#00a65a;
    }
}
</style>

<footer>
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2020 Deni Wardiman - Mitrais Test.</strong> All rights
    reserved.
</footer>