package com.mitrais.test.security;

import com.mitrais.test.model.UserCms;
import com.mitrais.test.repository.UserCmsRepository;
import org.springframework.beans.factory.annotation.Autowired;
/* COMMENT */
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
/* COMMENT */
import org.springframework.stereotype.Service;

import java.util.List;

@Service("customAccountDetailsService")
public class CustomAccountDetailsService /* COMMENT */ implements UserDetailsService /* COMMENT */{

	private final UserCmsRepository userCmsRepository;

	@Autowired
    public CustomAccountDetailsService(UserCmsRepository userCmsRepository) {
        this.userCmsRepository = userCmsRepository;
    }

    /* COMMENT */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserCms email = userCmsRepository.findByEmail(username);

		if (null == email) {
			throw new UsernameNotFoundException("No account present with email: "+username);
		} else {
			List<String> userRoles = userCmsRepository.findRoleById(email.getRole().getId());
			return new CustomAccountDetails(email,userRoles);
		}
	}
	/* COMMENT */
		
}
