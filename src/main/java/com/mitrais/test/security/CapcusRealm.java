package com.mitrais.test.security;

//import org.apache.shiro.authc.AuthenticationException;
//import org.apache.shiro.authc.AuthenticationInfo;
//import org.apache.shiro.authc.AuthenticationToken;
//import org.apache.shiro.authc.SimpleAuthenticationInfo;
//import org.apache.shiro.authz.AuthorizationInfo;
//import org.apache.shiro.authz.SimpleAuthorizationInfo;
//import org.apache.shiro.realm.AuthorizingRealm;
//import org.apache.shiro.subject.PrincipalCollection;
//import org.apache.shiro.util.ByteSource;


//public class CapcusRealm extends AuthorizingRealm {
//
//    @Resource
//    private UserCmsRepository userInfoService;

//    @Override
//    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
//
//        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
//        UserCms userInfo = (UserCms) principals.getPrimaryPrincipal();
//        authorizationInfo.addRole(userInfo.role.name);
//        for(RoleFeature roleFeature : userInfo.getRole().featureList){
//            authorizationInfo.addStringPermission(roleFeature.feature.code);
//        }
//        return authorizationInfo;
//    }
//
//    @Override
//    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
//
//        String username = (String)token.getPrincipal();
//
//        UserCms userInfo = userInfoService.findByEmail(username);
//        if(userInfo == null) {
//            return null;
//        }
//
//        return new SimpleAuthenticationInfo(
//                userInfo.getEmail(),
//                userInfo.getPassword(),
//                ByteSource.Util.bytes(userInfo.getCredentialsSalt()),
//                getName()
//        );
//    }
//}