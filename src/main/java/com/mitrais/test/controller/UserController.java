package com.mitrais.test.controller;

import java.security.Principal;

import com.mitrais.test.model.BaseResponse;
import com.mitrais.test.model.Role;
import com.mitrais.test.model.UserCms;
import com.mitrais.test.repository.UserCmsRepository;
import com.mitrais.test.service.RoleService;
import com.mitrais.test.service.UserCmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class UserController {
    private final PasswordEncoder passwordEncoder;
    private final UserCmsService userCmsService;
    private final RoleService roleService;
    private final UserCmsRepository userCmsRepositorye;

    @Autowired
    public UserController(UserCmsService userCmsService, RoleService roleService, PasswordEncoder passwordEncoder, UserCmsRepository userCmsRepositorye) {
        this.userCmsService = userCmsService;
        this.roleService = roleService;
        this.passwordEncoder = passwordEncoder;
        this.userCmsRepositorye = userCmsRepositorye;
    }
    
    @RequestMapping(value="/login", method=RequestMethod.GET)
    public String login(Principal principal) {
        return principal == null ?  "login" : "redirect:/home"; 
    }

    @RequestMapping(value="/register-form", method=RequestMethod.GET)
    public ModelAndView register_form() {
        ModelAndView model = new ModelAndView();
        model.setViewName("registration");
        return model;

    }

    @RequestMapping(value = "/save-create", method = RequestMethod.POST)
    public ModelAndView save(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView(new RedirectView("/login", true));
        String message;
        Boolean status = true;
        message = checkPhoneNumber(request.getParameter("mobileNumber"));
        if (message != null){
            System.out.println("masuk");
            status = false;
        }
        Role role = null;
        role = roleService.findOne("2fc19129-1c9b-4778-933a-f847eab32088");
        System.out.println(role);

        if (status) {
            Boolean isActive = true;
            UserCms user = new UserCms(passwordEncoder.encode(request.getParameter("password")), request.getParameter("firstName"), request.getParameter("lastName"),
                    request.getParameter("email"), role, isActive);
            message = "Data success insert";
            userCmsService.save(user);
        }

        if (status) {
            mav.addObject("msg", message);
        } else {
            System.out.println("ERROR");
            System.out.println(message);
            mav.addObject("err", "sdfasdfas");
        }
        return mav;
    }

    private String checkPhoneNumber(String phoneNumber){
        UserCms mobileNumber = userCmsRepositorye.findByPhone(phoneNumber);
        System.out.println(mobileNumber);
        return "";
    }
}
