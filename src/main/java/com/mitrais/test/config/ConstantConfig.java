package com.mitrais.test.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:application.properties")
public class ConstantConfig {
    @Autowired
    private Environment env;

    private static ConstantConfig instance = null;

    private String baseUrl = null;
    private String imagePath = null;
    private String fcmKey = null;
    private String sejalanApiKey = null;
    private String fixTimezone = null;
    private String serverTimezone = null;
    private String urlTransactionService = null;
    private String urlTransportationService = null;
    private String urlMasterService = null;
    private String urlRideSharingService = null;
    private String urlPromoService = null;

    @Bean
    public static ConstantConfig getInstance() {
        if (instance == null) {
            instance = new ConstantConfig();
        }

        return instance;
    }

    public String getBaseUrl() {
        if (baseUrl  == null){
            baseUrl  = env.getProperty("capcus.base.url");
        }
        return baseUrl ;
    }

    public String getImagePath() {
        if (imagePath == null){
            imagePath = env.getProperty("capcus.images.path");
        }
        return imagePath;
    }

    public String getFcmKey() {
        if (fcmKey == null){
            fcmKey = env.getProperty("capcus.firebase.key");
        }
        return fcmKey;
    }


    public String getSejalanApiKeyWeb() {
        if (sejalanApiKey == null){
            sejalanApiKey = env.getProperty("capcus.api_key.web");
        }
        return sejalanApiKey;
    }

    public String getFixTimezone() {
        if(fixTimezone == null) {
            fixTimezone = env.getProperty("capcus.fixtimezone");
            if(fixTimezone == null) {
                fixTimezone = "UTC";
            }
        }
        return fixTimezone;
    }

    public String getServerTimezone() {
        if (serverTimezone == null) {
            serverTimezone = env.getProperty("server.timezone");
        }
        return serverTimezone;
    }

    public String getUrlTransactionService() {
        if (urlTransactionService == null) {
            urlTransactionService = env.getProperty("transaction.service.url");
        }
        return urlTransactionService;
    }

    public String getUrlTransportationService() {
        if (urlTransportationService == null) {
            urlTransportationService = env.getProperty("transportation.service.url");
        }
        return urlTransportationService;
    }

    public String getUrlMasterService() {
        if (urlMasterService == null) {
            urlMasterService = env.getProperty("master.service.url");
        }
        return urlMasterService;
    }

    public String getUrlRideSharingService() {
        if (urlRideSharingService == null) {
            urlRideSharingService = env.getProperty("rideSharing.service.url");
        }
        return urlRideSharingService;
    }

    public String getUrlPromoService() {
        if (urlPromoService == null) {
            urlPromoService = env.getProperty("promo.service.url");
        }
        return urlPromoService;
    }
}
