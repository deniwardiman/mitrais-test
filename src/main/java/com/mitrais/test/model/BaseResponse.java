package com.mitrais.test.model;

public class BaseResponse<T> {
    private MetaInfo meta;
    private String message;
    private Object data;

    public BaseResponse() {
        super();
    }

    public void setBaseResponse(int total, int offset, int limit, String message, Object data) {
        MetaInfo metaInfo = new MetaInfo(total, offset, limit);
        this.meta = metaInfo;
        this.message = message;
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MetaInfo getMeta() {
        return meta;
    }

    public void setMeta(int total, int offset, int limit) {
        this.meta = new MetaInfo(total, offset, limit);
    }
}