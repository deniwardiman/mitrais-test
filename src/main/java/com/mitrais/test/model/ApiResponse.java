package com.mitrais.test.model;

public class ApiResponse{
    private Integer status;
    private String message;
    private Object results;

    public ApiResponse() {
    }

    public ApiResponse(Integer status, String message, Object results) {
        this.status = status;
        this.message = message;
        this.results = results;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getResults() {
        return results;
    }

    public void setResults(Object results) {
        this.results = results;
    }
}