package com.mitrais.test.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "users_cms")
public class UserCms {
    public static final Boolean GENDER_WOMAN = false;
    public static final Boolean GENDER_MAN = true;
    public static final Boolean INACTIVE = false;
    public static final Boolean ACTIVE = true;
    private static final long serialVersionUID = 4480994239348052016L;

    @Id
    @Column(length = 36)
    private String id;

    @Column(name = "created_at", updatable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "UTC")
    @JsonIgnore
    public Date createdAt;

    @Column(name = "updated_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "UTC")
    @JsonIgnore
    public Date updatedAt;

    @Column(name = "is_deleted")
    @JsonIgnore
    public boolean isDeleted;

    @JsonIgnore
    @Column(length = 500)
    public String password;

    @JsonProperty("first_name")
    @Column(name = "first_name", length = 50)
    public String firstName;
    @Column(name = "last_name", length = 50)
    @JsonProperty("last_name")
    public String lastName;
    @Column(unique = true, length = 50)
    public String email;
    @JsonProperty("full_name")
    @Column(name = "full_name", length = 100)
    public String fullName;

    @Column(length = 50)
    public String phone;
    public Boolean gender;
    @Temporal(TemporalType.DATE)
    @Column(name = "birth_date")
    @JsonProperty("birth_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "UTC")
    public Date birthDate;

    @JsonProperty("is_active")
    @Column(name = "is_active")
    public boolean isActive;

    @ManyToOne
    public Role role;

    public UserCms() {

    }

    public UserCms(String password, String firstName, String lastName, String email, Role role, Boolean isActive) {
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.fullName = firstName + " " + lastName;
        this.isActive = isActive;
        this.id = UUID.randomUUID().toString();
        this.isDeleted = false;
        this.role = role;
        this.createdAt = new Date();
    }

    public UserCms(UserCms userCms) {
        this.password = userCms.password;
        this.firstName = userCms.firstName;
        this.lastName = userCms.lastName;
        this.email = userCms.email;
        this.fullName = userCms.firstName + " " + lastName;
        this.isActive = true;
        this.id = UUID.randomUUID().toString();
        this.isDeleted = false;
        this.role = userCms.role;
        this.createdAt = new Date();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

}
