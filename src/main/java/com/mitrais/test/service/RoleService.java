package com.mitrais.test.service;

import com.mitrais.test.model.Role;
import com.mitrais.test.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleService {

	private final RoleRepository roleRepository;

	@Autowired
	public RoleService(RoleRepository roleRepository) {
		this.roleRepository = roleRepository;
	}

	public Page<Role> findAll(Pageable pageable) {
		return roleRepository.findAll(pageable);
	}

	public List<Role> findAll() {
		return roleRepository.findAll();
	}

	public Role findOne(String id) {
		return roleRepository.findOne(id);
	}

	public void save(Role role) {
		roleRepository.save(role);
	}

	public void update(Role role) {
		roleRepository.saveAndFlush(role);
	}

	public void delete(String id) {
		Role role = findOne(id);
		roleRepository.delete(role);
	}
}
