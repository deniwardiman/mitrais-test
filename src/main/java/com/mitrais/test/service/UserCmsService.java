package com.mitrais.test.service;


import com.mitrais.test.model.UserCms;
import com.mitrais.test.repository.UserCmsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class UserCmsService {
        private static final Logger logger = LoggerFactory.getLogger(UserCmsService.class);
        
        @Autowired 
        private HttpSession httpSession;
        
	private final UserCmsRepository userCmsRepository;

	@Autowired
	public UserCmsService(UserCmsRepository userCmsRepository) {
		this.userCmsRepository = userCmsRepository;
	}


	public void save(UserCms userCms) {
		userCmsRepository.save(userCms);
	}
}