package com.mitrais.test.repository;

import com.mitrais.test.model.UserCms;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UserCmsRepository extends JpaRepository<UserCms, String> {

    @Query("select a from UserCms a where a.email=?1 and a.isActive=true")
    UserCms findByEmail(String email);
    UserCms findByPhone(String phoneNumber);

    @Query("select a.name from Role a where a.id=?1")
    List<String> findRoleById(String id);

    @Query(value = "select b from UserCms b where (b.email like %:name% OR b.fullName like %:name%)")
    Page<UserCms> search(Pageable pageable, @Param("name") String keyword);

    @Query("select a from UserCms a where a.firstName=?1 and a.isActive=true")
    UserCms findByUsername(String firstName);

    @Transactional
    @Query(value = "update users_cms set password = :newPassword where id = :userId and is_active=true", nativeQuery = true)
    void udpdatePassword(@Param("newPassword") String password, @Param("userId") String userId);
}
