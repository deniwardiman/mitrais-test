package com.mitrais.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class MitraisCmsApplication extends SpringBootServletInitializer {

	private static final Logger logger = LoggerFactory.getLogger(MitraisCmsApplication.class);

	/**
	 * todo update module users sort by update at [DONE]
	 * todo add new feature trip active which join to order tmp
	 * todo add new feature to finish/cancel trip (on trip active only, not applicable to module sejalan trips)
	 * todo add new feature user search log [DONE]
	 * */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(MitraisCmsApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(MitraisCmsApplication.class, args);
	}
}
